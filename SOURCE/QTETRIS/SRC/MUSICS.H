/* TETRIS Queen - Copyright (C) 1999-2002 by David A. Capello
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */



static char *music_name[] =
{
  "Bohemian Rhapsody",
  "Another One Bites The Dust",
  "Killer Queen",
  "Fat Bottomed Girls",
  "Bicycle Race",
  "You're My Best Friend",
  "Don't Stop Me Now",
  "Save Me",
  "Crazy Little Thing Called Love",
  "Somebody To Love",
  "Now I'm Here",
  "Good Old Fashioned Lover Boy",
  "Play The Game",
  "Flash",
  "Seven Seas Of Rhye",
  "We Will Rock You",
  "We Are The Champions",
  "A Kind Of Magic",
  "Under Pressure",
  "Radio Ga Ga",
  "I Want It All",
  "I Want To Break Free",
  "Innuendo",
  "It's A Hard Life",
  "Breakthru",
  "Who Wants To Live Forever",
  "Headlong",
  "The Miracle",
  "I'm Going Slightly Mad",
  "The Invisible Man",
  "Hammer To Fall",
  "Friends Will Be Friends",
  "The Show Must Go On",
  "One Vision"
};

