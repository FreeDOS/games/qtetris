# TETRIS Queen

A clone of the TETRIS game, and a tribute to the Queen band at the same time.

# Contributing

**Would you like to contribute to FreeDOS?** The programs listed here are a great place to start. Most of these do not have a maintainer anymore and need your help to make them better. Here's how to get started:

* __Maintainers__: Let us know if you'd like to take on one of these programs, and we can provide access to the source code repository here. Please use the FreeDOS package structure when you make new releases, including all executables, source code, and metadata.

* __New developers__: We can extend access for you to track issues here.

* __Translators__: Please submit to the [FD-NLS Project](https://github.com/shidel/fd-nls).

_*Make sure to check all source code licenses, especially for any code you might reuse from other projects to improve these programs. Note that not all open source licenses are the same or compatible with one another. (For example, you cannot reuse code covered under the GNU GPL in a program that uses the BSD license.)_

## QTETRIS.LSM

<table>
<tr><td>title</td><td>TETRIS Queen</td></tr>
<tr><td>version</td><td>1.4.1a</td></tr>
<tr><td>entered&nbsp;date</td><td>2013-07-16</td></tr>
<tr><td>description</td><td>A clone of the TETRIS game, and tribute to the band Queen</td></tr>
<tr><td>keywords</td><td>tetris</td></tr>
<tr><td>author</td><td>David A. Capello</td></tr>
<tr><td>primary&nbsp;site</td><td>http://qtetris.sourceforge.net</td></tr>
<tr><td>platforms</td><td>DOS</td></tr>
<tr><td>copying&nbsp;policy</td><td>GNU General Public License, Version 2</td></tr>
<tr><td>summary</td><td>A clone of the TETRIS game, and a tribute to the Queen band at the same time.</td></tr>
</table>
